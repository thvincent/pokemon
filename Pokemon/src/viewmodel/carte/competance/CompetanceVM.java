package viewmodel.carte.competance;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.carte.competance.Competance;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class CompetanceVM implements PropertyChangeListener {
    private Competance model;

    public Competance getModel() {
        return model;
    }

    private StringProperty competance = new SimpleStringProperty();
        public String getCompetance() { return competance.get(); }
        public StringProperty competanceProperty() { return competance; }
        public void setCompetance(String competance) { this.competance.set(competance); }

    public CompetanceVM(Competance model) {
        this.model = model;
        competance.set(model.getCompetance());
        competance.addListener((__,___,newV)->model.setCompetance(newV));
        model.ajouterListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals(Competance.PROP_COMPETANCE)){
            competance.set((String)propertyChangeEvent.getNewValue());
        }
    }
}
