package viewmodel.carte;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.carte.Pokemon;
import model.carte.Type.Type;
import model.carte.competance.Competance;
import viewmodel.carte.competance.CompetanceVM;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class PokemonVM implements PropertyChangeListener {
    private Pokemon model;

    public Pokemon getModel() {
        return model;
    }

    private ObservableList<CompetanceVM> competancesObs = FXCollections.observableArrayList();
    private ListProperty<CompetanceVM> competances = new SimpleListProperty<>(competancesObs);
        public ObservableList<CompetanceVM> getCompetances() { return competances.get(); }
        public ReadOnlyListProperty<CompetanceVM> competancesProperty() { return competances; }
        public void setCompetances(ObservableList<CompetanceVM> competances) { this.competances.set(competances); }

    private StringProperty pokemon = new SimpleStringProperty();
        public String getPokemon() { return pokemon.get(); }
        public StringProperty pokemonProperty() { return pokemon; }
        public void setPokemon(String pokemon) { this.pokemon.set(pokemon); }

    private Type type;
        public Type getType() { return type; }
        public void setType(Type type) { this.type = type; model.setType(type);}

    public PokemonVM(Pokemon model) {
        this.model = model;
        model.getCompetances().forEach(competance -> ajouterCompetanceVM(new CompetanceVM(competance)));
        type = model.getType();
        pokemon.set(model.getPokemon());
        pokemon.addListener((__,___,newV)->model.setPokemon(newV));
        model.ajouterListener(this);
    }

    private void ajouterCompetanceVM(CompetanceVM c){
        competances.add(c);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals(Pokemon.PROP_POKEMON)){
            pokemon.set((String)propertyChangeEvent.getNewValue());
        }
        if(propertyChangeEvent.getPropertyName().equals(Pokemon.PROP_COMPETANCE_ADD)){
            competances.add(((IndexedPropertyChangeEvent)propertyChangeEvent).getIndex(), new CompetanceVM((Competance)propertyChangeEvent.getNewValue()));
        }
        if(propertyChangeEvent.getPropertyName().equals(Pokemon.PROP_COMPETANCE_REMOVE)){
            competances.remove(((IndexedPropertyChangeEvent)propertyChangeEvent).getIndex());
        }
    }
}
