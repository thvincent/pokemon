package viewmodel;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Deck;
import model.carte.Pokemon;
import model.carte.Type.Type;
import model.carte.competance.Competance;
import viewmodel.carte.PokemonVM;
import viewmodel.carte.competance.CompetanceVM;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class DeckVM implements PropertyChangeListener {
    private Deck model;

    public Deck getModel() {
        return model;
    }

    private ObservableList<PokemonVM> pokemonsObs = FXCollections.observableArrayList();
    private ListProperty<PokemonVM> pokemons = new SimpleListProperty<>(pokemonsObs);
        public ObservableList<PokemonVM> getPokemons() { return pokemons.get(); }
        public ListProperty<PokemonVM> pokemonsProperty() { return pokemons; }
        public void setPokemons(ObservableList<PokemonVM> pokemons) { this.pokemons.set(pokemons); }

    public DeckVM(Deck model) {
        this.model = model;
        model.getPokemons().forEach(pokemon -> ajouterPokemonVM(new PokemonVM(pokemon)));
        model.ajouterListener(this);
    }

    private void ajouterPokemonVM(PokemonVM pokemonVM) {
        pokemons.add(pokemonVM);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if(propertyChangeEvent.getPropertyName().equals(Deck.PROP_POKEMON_ADD)){
            pokemons.add(((IndexedPropertyChangeEvent)propertyChangeEvent).getIndex(), new PokemonVM((Pokemon) propertyChangeEvent.getNewValue()));
        }
        if(propertyChangeEvent.getPropertyName().equals(Deck.PROP_POKEMON_REMOVE)){
            pokemons.remove(((IndexedPropertyChangeEvent)propertyChangeEvent).getIndex());
        }
    }

    public void ajouterPokemon() {
        model.ajouterPokemon(new Pokemon("Nouveau", Type.NONE));
    }

    public void supprimerPokemon(PokemonVM pokemonVM) {
        model.supprimerPokemon(pokemonVM.getModel());
    }

    public void ajouterCompetance(int selectedItem) {
        model.getPokemons().get(selectedItem).ajouterCompetance(new Competance("Nouveau"));
    }

    public void modifierCompetance(int selectedIndexPokemon, int selectedIndexCompetance, String text) {
        model.getPokemons().get(selectedIndexPokemon).getCompetances().get(selectedIndexCompetance).setCompetance(text);
    }

    public void supprimerCompetance(int selectedIndexPokemon, CompetanceVM competanceVM) {
        model.getPokemons().get(selectedIndexPokemon).supprimerCompetance(competanceVM.getModel());
    }
}
