package donnee.StubAssembly;

import donnee.Strategie;
import model.Deck;
import model.carte.Pokemon;
import model.carte.Type.Type;
import model.carte.competance.Competance;

public class StubDeck implements Strategie {
    @Override
    public void sauvegarder(Deck deck) {}

    @Override
    public Deck charger() {
        Deck deck = new Deck();

        Pokemon p1 = new Pokemon("Pikachu", Type.FEU);
        Pokemon p2 = new Pokemon("Tortank", Type.EAU);

        p1.ajouterCompetance(new Competance("Tonnere"));
        p1.ajouterCompetance(new Competance("Foudre"));

        p2.ajouterCompetance(new Competance("HydroCanon"));
        p2.ajouterCompetance(new Competance("Aqua"));

        deck.ajouterPokemon(p1);
        deck.ajouterPokemon(p2);

        return deck;
    }
}
