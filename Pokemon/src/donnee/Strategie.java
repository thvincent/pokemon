package donnee;

import model.Deck;

import java.io.IOException;

public interface Strategie {
    void sauvegarder(Deck deck) throws IOException;
    Deck charger() throws IOException, ClassNotFoundException;
}
