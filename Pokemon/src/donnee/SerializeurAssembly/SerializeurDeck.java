package donnee.SerializeurAssembly;

import donnee.Strategie;
import model.Deck;

import java.io.*;

public class SerializeurDeck implements Strategie {
    @Override
    public void sauvegarder(Deck deck) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("save.bin"));
        oos.writeObject(deck);
    }

    @Override
    public Deck charger() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("save.bin"));
        return (Deck) ois.readObject();
    }
}
