package view.controleur;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.carte.Type.Type;
import view.fxml.CelluleCompetanceVM;
import view.fxml.CellulePokemonVM;
import viewmodel.DeckVM;
import viewmodel.carte.PokemonVM;
import viewmodel.carte.competance.CompetanceVM;

import java.io.IOException;

public class PrincipalControleur {
    private final DeckVM viewmodel;

    public DeckVM getViewmodel() {
        return viewmodel;
    }

    @FXML
    private ListView<PokemonVM> pokemonListView;
    @FXML
    private ListView<CompetanceVM> competanceListView;
    @FXML
    private TextField pokemonTextField;
    @FXML
    private Label typeLabel;
    @FXML
    private ChoiceBox<Type> typeChoiceBox;

    public PrincipalControleur(DeckVM viewmodel) {
        this.viewmodel = viewmodel;
    }

    @FXML
    private void initialize(){
        pokemonListView.setCellFactory((__)->new CellulePokemonVM());
        competanceListView.setCellFactory((__)->new CelluleCompetanceVM());
        pokemonListView.itemsProperty().bind(viewmodel.pokemonsProperty());
        pokemonListView.getSelectionModel().selectedItemProperty().addListener((__,oldV,newV)->initializeBindingPokemon(oldV,newV));
        typeChoiceBox.setItems(FXCollections.observableArrayList(Type.FEU,Type.EAU,Type.PLANTE,Type.NONE));
        typeChoiceBox.getSelectionModel().selectedItemProperty().addListener((__,___,newV)->changementTypePokemon(newV));
    }

    private void changementTypePokemon(Type newV) {
        pokemonListView.getSelectionModel().getSelectedItem().setType(newV);
        pokemonListView.refresh();
    }

    private void initializeBindingPokemon(PokemonVM oldV, PokemonVM newV) {
        if(oldV != null){
            pokemonTextField.textProperty().unbindBidirectional(oldV.pokemonProperty());
            competanceListView.itemsProperty().unbind();
        }
        if(newV != null){
            pokemonTextField.textProperty().bindBidirectional(newV.pokemonProperty());
            competanceListView.itemsProperty().bind(newV.competancesProperty());
            typeChoiceBox.setValue(newV.getType());
            typeLabel.textProperty().bind(typeChoiceBox.getSelectionModel().selectedItemProperty().asString());
        }
    }

    @FXML
    private void ajouterPokemon(){
        viewmodel.ajouterPokemon();
    }

    @FXML
    private void supprimerPokemon(){
        viewmodel.supprimerPokemon(pokemonListView.getSelectionModel().getSelectedItem());
        competanceListView.setItems(null);
        pokemonTextField.setText("");
    }

    @FXML
    private void ajouterCompetance(){
        viewmodel.ajouterCompetance(pokemonListView.getSelectionModel().getSelectedIndex());
    }

    @FXML
    private void modifierCompetance() throws IOException {
        System.out.println(competanceListView.getSelectionModel().getSelectedItem().getCompetance());
        Stage stage = new Stage();
        CompetanceControleur controleur = new CompetanceControleur(competanceListView.getSelectionModel().getSelectedItem().getCompetance());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FenetreCompetance.fxml"));
        loader.setController(controleur);
        Scene scene = new Scene(loader.load());
        scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.showAndWait();
        if(controleur.isSave()){
            viewmodel.modifierCompetance(pokemonListView.getSelectionModel().getSelectedIndex(), competanceListView.getSelectionModel().getSelectedIndex(), controleur.getCompetanceTextField().getText());
        }
    }

    @FXML
    private void supprimerCompetance(){
        viewmodel.supprimerCompetance(pokemonListView.getSelectionModel().getSelectedIndex(), competanceListView.getSelectionModel().getSelectedItem());
    }
}
