package view.controleur;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CompetanceControleur {
    private boolean save = false;
    @FXML
    private TextField competanceTextField;
    private String valeur;

    public boolean isSave() {
        return save;
    }

    public TextField getCompetanceTextField() {
        return competanceTextField;
    }

    public CompetanceControleur(String valeur) {
        this.valeur = valeur;
    }

    @FXML
    private void initialize(){
        competanceTextField.setText(valeur);
    }

    @FXML
    private void sauver() {
        save = true;
        annuler();
    }

    @FXML
    private void annuler() {
        Stage stage = (Stage) competanceTextField.getScene().getWindow();
        stage.close();
    }
}
