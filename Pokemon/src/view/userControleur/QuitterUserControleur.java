package view.userControleur;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;

public class QuitterUserControleur extends HBox {
    @FXML
    private Button quitterButton;

    public QuitterUserControleur() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userControl/ControlQuitter.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        loader.load();
    }

    @FXML
    private void quitter(){
        Stage stage = (Stage) quitterButton.getScene().getWindow();
        stage.close();
    }
}
