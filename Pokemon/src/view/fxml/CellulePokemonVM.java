package view.fxml;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import viewmodel.carte.PokemonVM;

public class CellulePokemonVM extends ListCell<PokemonVM> {
    @Override
    protected void updateItem(PokemonVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            HBox box = new HBox();
            ImageView icon = new ImageView();
            Label label = new Label();
            switch(item.getType()){
                case EAU:
                    icon.setImage(new Image("/image/type/eau.png"));
                    break;
                case FEU:
                    icon.setImage(new Image("/image/type/feu.png"));
                    break;
                case PLANTE:
                    icon.setImage(new Image("/image/type/plante.png"));
                    break;
                case NONE:
                default:
                    icon.setImage(new Image("/image/type/pokeball.png"));
                    break;
            }
            label.setText(item.getPokemon());
            box.getChildren().add(icon);
            box.getChildren().add(label);
            setGraphic(box);
        }
        else{
            setGraphic(null);
        }
    }
}
