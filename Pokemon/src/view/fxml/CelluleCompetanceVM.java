package view.fxml;

import javafx.scene.control.ListCell;
import viewmodel.carte.competance.CompetanceVM;

public class CelluleCompetanceVM extends ListCell<CompetanceVM> {
    @Override
    protected void updateItem(CompetanceVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            textProperty().bind(item.competanceProperty());
        }
        else{
            textProperty().unbind();
            setText("");
        }
    }
}
