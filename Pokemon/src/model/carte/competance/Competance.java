package model.carte.competance;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class Competance implements Serializable {
    public static final transient String PROP_COMPETANCE = "Propriété Compétance";
    private transient PropertyChangeSupport support = new PropertyChangeSupport(this);

    public PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    private String competance;

    public String getCompetance() {
        return competance;
    }

    public void setCompetance(String competance) {
        String old = this.competance;
        this.competance = competance;
        getSupport().firePropertyChange(PROP_COMPETANCE, old, competance);
    }

    public Competance(String competance) {
        this.competance = competance;
    }

    public void ajouterListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }
}
