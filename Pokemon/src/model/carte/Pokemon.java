package model.carte;

import model.carte.Type.Type;
import model.carte.competance.Competance;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pokemon implements Serializable {
    public static final transient String PROP_POKEMON = "Propriété pokémon";
    public static final transient String PROP_TYPE = "Propriété type";
    public static final transient String PROP_COMPETANCE_ADD = "Propriété compétance ajouter";
    public static final transient String PROP_COMPETANCE_REMOVE = "Propriété compétance supprimer";
    private transient PropertyChangeSupport support = new PropertyChangeSupport(this);

    public PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    private final List<Competance> competances = new ArrayList<>();
    private String pokemon;
    private Type type;

    public String getPokemon() {
        return pokemon;
    }

    public void setPokemon(String pokemon) {
        String old = this.pokemon;
        this.pokemon = pokemon;
        getSupport().firePropertyChange(PROP_POKEMON, old, pokemon);
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        Type old = this.type;
        this.type = type;
        getSupport().firePropertyChange(PROP_TYPE, old, type);
    }

    public List<Competance> getCompetances() {
        return Collections.unmodifiableList(competances);
    }

    public Pokemon(String pokemon, Type type) {
        this.pokemon = pokemon;
        this.type = type;
    }

    public void ajouterCompetance(Competance c){
        competances.add(c);
        getSupport().fireIndexedPropertyChange(PROP_COMPETANCE_ADD, competances.indexOf(c), null, c);
    }

    public void supprimerCompetance(Competance c){
        int index = competances.indexOf(c);
        competances.remove(c);
        getSupport().fireIndexedPropertyChange(PROP_COMPETANCE_REMOVE, index, c, null);
    }

    public void ajouterListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }
}
