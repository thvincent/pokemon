package model;

import model.carte.Pokemon;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck implements Serializable {
    public static final transient String PROP_POKEMON_ADD = "Propriété pokémon ajouter";
    public static final transient String PROP_POKEMON_REMOVE = "Propriété pokémon supprimer";
    private transient PropertyChangeSupport support = new PropertyChangeSupport(this);

    public PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    private final List<Pokemon> pokemons = new ArrayList<>();

    public List<Pokemon> getPokemons() {
        return Collections.unmodifiableList(pokemons);
    }

    public Deck() {}

    public void ajouterPokemon(Pokemon p){
        pokemons.add(p);
        getSupport().fireIndexedPropertyChange(PROP_POKEMON_ADD, pokemons.indexOf(p), null, p);
    }

    public void supprimerPokemon(Pokemon p){
        int index = pokemons.indexOf(p);
        pokemons.remove(p);
        getSupport().fireIndexedPropertyChange(PROP_POKEMON_REMOVE, index, p, null);
    }

    public void ajouterListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }
}
