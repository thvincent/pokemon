import donnee.SerializeurAssembly.SerializeurDeck;
import donnee.StubAssembly.StubDeck;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.controleur.PrincipalControleur;
import viewmodel.DeckVM;

import java.io.IOException;

public class Main extends Application {
    private PrincipalControleur controleur;

    @Override
    public void start(Stage stage) throws Exception {
        DeckVM deckVM;
        try {
            deckVM = new DeckVM(new SerializeurDeck().charger());
        } catch (IOException | ClassNotFoundException e) {
            deckVM = new DeckVM(new StubDeck().charger());
        }
        //deckVM = new DeckVM(new StubDeck().charger());
        controleur = new PrincipalControleur(deckVM);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FenetrePrincipal.fxml"));
        loader.setController(controleur);
        Scene scene = new Scene(loader.load());
        scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        new SerializeurDeck().sauvegarder(controleur.getViewmodel().getModel());
    }
}
